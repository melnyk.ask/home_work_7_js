// 1. Опишіть своїми словами як працює метод forEach.
// Метод forEach виконує для кожного елементу масиву функцію.
// Функція виконується один раз для кожного елементу масиву. Може
// приймати аргументи - значення елементу масива, індекс елемента і сам масив.
// Метод не змінює масив, який оброблює. Метод повертає undefined.

// 2. Як очистити масив?
// Просто присвоїти довжині масиву нуль:
// let arr = [1, 2, 3];
// arr.length = 0;
// в результаті масив буде [].

// 3. Як можна перевірити, що та чи інша змінна є масивом?
// Використати перевірку відповідним методом:
// Array.isArray(arr);
// даний метод поверне true у випадку, якщо змінна є масивом. І false, якщо ні.

// 7.1

function filterBy(array = [], data_type) {
    let arr = [];
    let i = 0;
    array.forEach(
        function (elem) {
            if (typeof (elem) !== data_type) { 
                    arr[i] = elem;
                    i++;          
            }
        }
    );
    return arr;
}

// 7.2

function filterBy2(array = [], data_type) {
    let arr = array.filter(
        //(elem) => typeof (elem) !== data_type
        function (elem) {
            if (typeof (elem) !== data_type) { 
                return true;             
            }         
        }        
    );
    return arr;
}


// let arr = ['hello', 'world', 23, '23', null, {}];
// let types = ["string", "object", "number", "boolean"];
// types.forEach(elem => console.log(filterBy(arr, elem)));
